plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("androidx.navigation.safeargs.kotlin")
    id("kotlin-parcelize")
    id("kotlin-kapt")
    id("dagger.hilt.android.plugin")
}

android {
    compileSdk = 32

    defaultConfig {
        applicationId = "com.philroy.animeretrofitfromscratch"
        minSdk = 24
        targetSdk = 32
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles (getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    // Navigation
    implementation(Deps.navigationFragment)
    implementation(Deps.navigationUI)

    // Coroutines
    implementation(Deps.coroutines)

    // Image processing Glide
    implementation(Deps.glide)
    implementation(project(mapOf("path" to ":lib_animes")))
    kapt(Deps.kaptGlideCompiler)

    // Dependencies: Dagger and Hilt
    implementation(Deps.hiltAndroid)
    kapt(Deps.hiltCompiler)

    implementation(Deps.coreKtx)
    implementation(Deps.appcompat)
    implementation(Deps.material)
    testImplementation(Deps.junitJunit)
    androidTestImplementation(Deps.androidxJunit)
    androidTestImplementation(Deps.espressoCore)

    implementation(project(":lib_animes"))
}