package com.philroy.animeretrofitfromscratch

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AnimeApplication: Application() {
}