package com.philroy.animeretrofitfromscratch.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.philroy.animeretrofitfromscratch.databinding.FragmentDetailBinding
import com.philroy.lib_animes.model.AnimeRepo
import com.philroy.lib_animes.util.Resource
import com.philroy.animeretrofitfromscratch.view.adapter.AnimeAdapter
import com.philroy.animeretrofitfromscratch.viewmodel.AnimeViewModel
import com.philroy.animeretrofitfromscratch.viewmodel.DetailViewModel
import com.philroy.animeretrofitfromscratch.viewmodel.factories.AnimeViewModelFactory
import com.philroy.animeretrofitfromscratch.viewmodel.factories.DetailViewModelFactory
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DetailFragment @Inject constructor(
): Fragment() {
    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!

    private val args by navArgs<DetailFragmentArgs>()
    private val viewModel: DetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDetailBinding.inflate(inflater, container, false)
        .also { _binding = it }
        .root



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getAnimeById(args.animeId)
        setObserver()
    }

    fun setObserver() = with(binding){
        viewModel.anime.observe(viewLifecycleOwner) { viewState ->
            when(viewState){
                is com.philroy.lib_animes.util.Resource.Loading -> {
                    // binding.progressCircular.show()
                }
                is com.philroy.lib_animes.util.Resource.Error -> {
                    // do nothing
                }
                is com.philroy.lib_animes.util.Resource.Success -> {
                    // binding.progressCircular.hide()
                    val anime = viewState.data[0]
                    ivCover.loadImage(anime.attributes.coverImage.tiny)
                    tvTitle.text = anime.attributes.canonicalTitle
                    tvRating.text = anime.attributes.averageRating
                    tvSynopsis.text = anime.attributes.synopsis

                }

            }
        }
    }
    private fun ImageView.loadImage(url: String) {
        Glide.with(context).load(url).into(this)
    }
}