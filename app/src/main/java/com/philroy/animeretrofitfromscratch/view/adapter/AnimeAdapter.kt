package com.philroy.animeretrofitfromscratch.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.philroy.animeretrofitfromscratch.databinding.ListItemBinding
import com.philroy.lib_animes.model.room.AnimeSummary
import javax.inject.Inject

class AnimeAdapter @Inject constructor(
    val itemClicked: (data: com.philroy.lib_animes.model.room.AnimeSummary) -> Unit
): RecyclerView.Adapter<AnimeAdapter.AnimeViewHolder>( ){

    private lateinit var animes: List<com.philroy.lib_animes.model.room.AnimeSummary>

    class AnimeViewHolder(
        private val binding: ListItemBinding
        ): RecyclerView.ViewHolder(binding.root) {

        fun apply(anime: com.philroy.lib_animes.model.room.AnimeSummary){
            binding.tvTitle.text = anime.canonicalTitle
            binding.ivThumbnail.loadImage(anime.posterImage)
        }

        private fun ImageView.loadImage(url: String) {
            Glide.with(context).load(url).into(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimeViewHolder {
        val binding = ListItemBinding.inflate(LayoutInflater.from(parent.context),parent, false)
        return AnimeViewHolder(binding).apply {
            binding.root.setOnClickListener {
                itemClicked(animes[adapterPosition])
            }
        }
    }

    override fun onBindViewHolder(holder: AnimeViewHolder, position: Int) {
        val item = animes[position]
        holder.apply(item)
    }

    override fun getItemCount(): Int {
        return animes.size
    }

    fun giveData(animes: List<com.philroy.lib_animes.model.room.AnimeSummary>) {
        this.animes = animes
    }

}