package com.philroy.animeretrofitfromscratch.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.philroy.animeretrofitfromscratch.databinding.FragmentFirstBinding
import com.philroy.animeretrofitfromscratch.view.adapter.AnimeAdapter
import com.philroy.animeretrofitfromscratch.viewmodel.AnimeViewModel
import com.philroy.lib_animes.util.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FirstFragment (): Fragment() {
    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!

    private val viewModel: AnimeViewModel by viewModels()

    private lateinit var adapter: AnimeAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFirstBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getAnimes()
        initObserver()
    }

    private fun initObserver(){
        viewModel.animes.observe(viewLifecycleOwner) {
            when(it){
                is Resource.Loading -> {
                    // binding.progressCircular.show()
                }
                is Resource.Error -> {
                    // do nothing
                }
                is Resource.Success -> {
                    // binding.progressCircular.hide()
                    binding.recyclerViewAnimes.layoutManager = LinearLayoutManager(requireContext())
                    binding.recyclerViewAnimes.adapter = AnimeAdapter(::itemClicked).apply {
                        giveData(it.data)
                    }
                }

            }
        }
    }
    fun itemClicked(anime: com.philroy.lib_animes.model.room.AnimeSummary) {
        val action = FirstFragmentDirections.actionFirstFragmentToDetailFragment(anime.id)
        findNavController().navigate(action)
    }
}










