package com.philroy.animeretrofitfromscratch.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.philroy.lib_animes.model.AnimeRepo
import com.philroy.lib_animes.model.room.AnimeSummary
import com.philroy.lib_animes.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AnimeViewModel @Inject constructor(
    private val repo: com.philroy.lib_animes.model.AnimeRepo
    ): ViewModel() {
    private var _animes: MutableLiveData<com.philroy.lib_animes.util.Resource<List<com.philroy.lib_animes.model.room.AnimeSummary>>> = MutableLiveData(
        com.philroy.lib_animes.util.Resource.Loading())
    val animes: LiveData<com.philroy.lib_animes.util.Resource<List<com.philroy.lib_animes.model.room.AnimeSummary>>> get() = _animes

    fun getAnimes() {
        viewModelScope.launch(Dispatchers.Main) {
            _animes.value = repo.getAnimes()
        }
    }
}