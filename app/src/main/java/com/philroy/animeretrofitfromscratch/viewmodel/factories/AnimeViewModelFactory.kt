package com.philroy.animeretrofitfromscratch.viewmodel.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.philroy.lib_animes.model.AnimeRepo
import com.philroy.animeretrofitfromscratch.viewmodel.AnimeViewModel
import dagger.hilt.android.AndroidEntryPoint

class AnimeViewModelFactory(
    private val repo: com.philroy.lib_animes.model.AnimeRepo
    ): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AnimeViewModel(repo) as T
    }
}