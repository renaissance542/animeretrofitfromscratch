package com.philroy.animeretrofitfromscratch.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.philroy.lib_animes.model.AnimeRepo
import com.philroy.lib_animes.model.models.Anime
import com.philroy.lib_animes.util.Resource
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor (
    private val repo: com.philroy.lib_animes.model.AnimeRepo
    ): ViewModel() {

    private var _animes: MutableLiveData<com.philroy.lib_animes.util.Resource<List<com.philroy.lib_animes.model.models.Anime>>> = MutableLiveData(
        com.philroy.lib_animes.util.Resource.Loading())
    val anime: LiveData<com.philroy.lib_animes.util.Resource<List<com.philroy.lib_animes.model.models.Anime>>> get() = _animes

    fun getAnimeById(id: String) {
        viewModelScope.launch(Dispatchers.Main) {
            _animes.value = repo.getAnimeById(id)
        }
    }
}