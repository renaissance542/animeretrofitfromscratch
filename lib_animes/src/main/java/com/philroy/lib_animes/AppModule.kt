package com.philroy.lib_animes

import android.content.Context
import androidx.room.Room
import com.philroy.lib_animes.model.remote.AnimeApi
import com.philroy.lib_animes.model.room.AnimeSummaryDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {


    @Singleton
    @Provides
    fun provideAnimeSummaryDatabase(
        @ApplicationContext context: Context
    ): com.philroy.lib_animes.model.room.AnimeSummaryDatabase = Room.databaseBuilder(
        context,
        AnimeSummaryDatabase::class.java,
        "Anime Summary Database"
        ).fallbackToDestructiveMigration()
        .build()

    @Singleton
    @Provides
    fun provideRetrofit(): AnimeApi =
        Retrofit
        .Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl("https://kitsu.io")
        .build()
        .create(AnimeApi::class.java)

    @Singleton
    @Provides
    fun provideAnimeSummaryDao(db: AnimeSummaryDatabase) = db.animeSummaryDao()

}