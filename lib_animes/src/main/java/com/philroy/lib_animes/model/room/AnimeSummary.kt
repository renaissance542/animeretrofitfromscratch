package com.philroy.lib_animes.model.room

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.parcelize.Parcelize
import javax.inject.Inject

@Parcelize
@Entity(tableName = "anime_summaries")
data class AnimeSummary (
    @PrimaryKey(autoGenerate = false)
    val id: String,
    val canonicalTitle: String? = "Missing Title",
    val posterImage: String,
): Parcelable