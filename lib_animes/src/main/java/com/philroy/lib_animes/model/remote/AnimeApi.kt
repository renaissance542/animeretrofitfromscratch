package com.philroy.lib_animes.model.remote

import com.philroy.lib_animes.model.models.Animes
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface AnimeApi {
    @GET ("/api/edge/anime")
    suspend fun getAnimes(): Response<Animes>

    @GET("/api/edge/anime")
    suspend fun getAnimeById(@Query("filter[id]") id: String): Response<Animes>

//    companion object {
//        val retrofitInstance by lazy {
//            Retrofit
//                .Builder()
//                .addConverterFactory(GsonConverterFactory.create())
//                .baseUrl("https://kitsu.io")
//                .build()
//                .create(AnimeApi::class.java)
//        }
//    }
}