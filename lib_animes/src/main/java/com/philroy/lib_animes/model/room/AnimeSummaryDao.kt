package com.philroy.lib_animes.model.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface AnimeSummaryDao {
    @Query("SELECT * FROM anime_summaries")
    suspend fun getAnimeSummaries(): List<AnimeSummary>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addAnimeSummaries(summaries: List<AnimeSummary>)
}