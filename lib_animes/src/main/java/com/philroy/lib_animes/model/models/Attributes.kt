package com.philroy.lib_animes.model.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Attributes (
    val canonicalTitle: String?,
    val posterImage: PosterImage,
    val coverImage: CoverImage,
    val averageRating: String?,
    val synopsis: String?

): Parcelable