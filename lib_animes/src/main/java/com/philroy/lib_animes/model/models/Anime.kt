package com.philroy.lib_animes.model.models

import android.os.Parcelable
import androidx.room.Entity
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Anime (
    val id: String,
    val attributes: Attributes
): Parcelable