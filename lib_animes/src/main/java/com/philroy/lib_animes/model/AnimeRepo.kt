package com.philroy.lib_animes.model

import android.util.Log
import com.philroy.lib_animes.model.models.Anime
import com.philroy.lib_animes.model.room.AnimeSummary
import com.philroy.lib_animes.model.remote.AnimeApi
import com.philroy.lib_animes.model.room.AnimeSummaryDatabase
import com.philroy.lib_animes.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class AnimeRepo @Inject constructor(
    val animeSummaryDatabase: AnimeSummaryDatabase,
    val animeApi: AnimeApi
) {

//    private val animeSummaryDao = AnimeSummaryDatabase.getDatabaseInstance(context).animeSummaryDao()
//    private val animeApi by lazy { AnimeApi.retrofitInstance }
    private val animeSummaryDao = animeSummaryDatabase.animeSummaryDao()

    suspend fun getAnimes(): Resource<List<AnimeSummary>> = withContext(Dispatchers.IO) {
        return@withContext try {
            val response = animeApi.getAnimes()
            val dbInfo = animeSummaryDao.getAnimeSummaries()
            if (response.isSuccessful && response.body()!!.data.size > dbInfo.size ){
                val summaries = response.body()!!.data.map {
                    AnimeSummary(
                        id = it.id,
                        canonicalTitle = it.attributes.canonicalTitle,
                        posterImage = it.attributes.posterImage.tiny
                    )
                }
                animeSummaryDao.addAnimeSummaries(summaries)
                Log.d("TAG", "Anime added from api")
                Resource.Success(summaries)
            } else {
                Log.d("TAG", "Anime added from database")
                Resource.Success(dbInfo)
            }
        } catch (e: Exception) {
            Resource.Error(e.localizedMessage?: "no localized error message")
        }
    }

    suspend fun getAnimeById(id: String): Resource<List<Anime>> = withContext(Dispatchers.IO) {
        return@withContext try {
            val response = animeApi.getAnimeById(id)
            if (response.isSuccessful && response.body() != null ){
                Resource.Success(response.body()!!.data)
            } else {
                Resource.Error("empty API response")
            }
        } catch (e: Exception) {
            Resource.Error(e.localizedMessage?: "no localized error message")
        }
    }
}