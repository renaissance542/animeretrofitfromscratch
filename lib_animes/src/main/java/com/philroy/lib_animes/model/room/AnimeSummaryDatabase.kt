package com.philroy.lib_animes.model.room

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [AnimeSummary::class], exportSchema = false, version = 69)
abstract class AnimeSummaryDatabase(): RoomDatabase() {
    abstract fun animeSummaryDao(): AnimeSummaryDao

}