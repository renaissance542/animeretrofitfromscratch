package com.philroy.lib_animes.model.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PosterImage(
    val tiny: String
): Parcelable
