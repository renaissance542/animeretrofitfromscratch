import Versions.coroutine
import Versions.hilt
import Versions.navigation
import Versions.retrofit
import Versions.room

object Versions {
    const val navigation = "2.4.2"
    const val room = "2.4.2"
    const val coroutine = "1.6.0"
    const val retrofit = "2.9.0"
    const val hilt = "2.38.1"
}

object Deps {

    // Navigation
    val navigationFragment ="androidx.navigation:navigation-fragment-ktx:$navigation"
    val navigationUI = "androidx.navigation:navigation-ui-ktx:$navigation"

    // Coroutines
    val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutine"

    // Retrofit
    val retrofitGsonConverter = "com.squareup.retrofit2:converter-gson:$retrofit"
    val retrofitRetrofit = "com.squareup.retrofit2:retrofit:$retrofit"

    // Image processing Glide
    val glide = "com.github.bumptech.glide:glide:4.13.1"
    val kaptGlideCompiler = "com.github.bumptech.glide:compiler:4.13.1"

    // Room SQLite
    val roomRuntime = "androidx.room:room-runtime:$room"
    val roomKtx = "androidx.room:room-ktx:$room"
    val roomComplier = "androidx.room:room-compiler:$room"

    // Dependencies: Dagger and Hilt
    val hiltAndroid = "com.google.dagger:hilt-android:$hilt"
    val hiltCompiler = "com.google.dagger:hilt-compiler:$hilt"

    val coreKtx = "androidx.core:core-ktx:1.7.0"
    val appcompat = "androidx.appcompat:appcompat:1.4.1"
    val material = "com.google.android.material:material:1.5.0"
    val junitJunit = "junit:junit:4.13.2"
    val androidxJunit = "androidx.test.ext:junit:1.1.3"
    val espressoCore = "androidx.test.espresso:espresso-core:3.4.0"
}